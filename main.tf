terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  # version for terraform
  required_version = ">= 1.2.0"

  #backend "s3" {
  #  bucket = "lab05-tfstate"
  #  key = "simple/terraform.tfstate"
  #  region = "eu-west-2"
  #}
}

# Configure the AWS Provider
provider "aws" {
  region = "eu-west-2"
}

locals {
  key_pair = "herasymchukDP-key"
}

# Create a VPC
resource "aws_instance" "app_server" {
  count = 2

  ami                    = data.aws_ami.ubuntu.id #"ami-0e5f882be1900e43b"
  instance_type          = var.instance_type
  key_name               = local.key_pair
  vpc_security_group_ids = [data.aws_security_group.selected.id] # ssh-http-https id

  tags = {
    Name = "Herasymchuk5Lab-count ${count.index}, instance ${var.instance_type}"
  }
}

/* ctrl+shift+/
import {
    to = aws_instance.app_server
    id = "i-12345678"
} */
