# EC2 Module

module "lab05" {
  source = "terraform-aws-modules/ec2-instance/aws"
  name   = "${local.name}-${terraform.workspace}" # Herasymchuk-lab5

  ami                = data.aws_ami.ubuntu.id
  ignore_ami_changes = true

  instance_type = local.instance_type
  key_name      = local.key_pair # module.key_pair.key_pair_name
  monitoring    = true

  availability_zone = data.aws_subnet.selected.availability_zone
  subnet_id         = data.aws_subnet.selected.id

  vpc_security_group_ids      = [data.aws_security_group.selected.id] # [module.security_group.security_group_id]  # "sg-12345678"
  associate_public_ip_address = true

  #user_data_base64 = base64encode(local.user_data)
  user_data_replace_on_change = false

  root_block_device = [
    {
      encrypted   = true
      volume_type = "gp3"
      throughput  = 200
      volume_size = 8
    },
  ]

  #   tags = {
  #     Terraform   = "true"
  #     Environment = "dev"
  #   }
  tags = merge({ "Environment" = terraform.workspace }, local.tags)
}

# Supporting Resources
#
# module "key_pair" {
#     source = "terraform-aws-modules/key_pair/aws"
#     key_name = local.name
#     create_private_key = true
# }

# module "security_group" {
#     source = "terraform-aws-modules/security-group/aws"
#     version = "~> 4.0"
#
#     name = "${local.name}-${terraform.workspace}"
#     description = "Security group for EC2 instance"
#     vpc_id = data.aws_vpc.default.id
#
#     ingress_cidr_blocks = ["0.0.0.0/0"]
#     ingress_rules = ["http-80-tcp", "https-443-tcp", "ssh-tcp"]
#     egress_rules = ["all-all"]
#
#     tags = merge({"Environment" = terraform.workspace }, local.tags)
# }