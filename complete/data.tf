data "aws_region" "current" {}

data "aws_availability_zones" "available" {}


# as in previous
data "aws_ami" "ubuntu" {
  most_recent = true


  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

data "aws_subnet" "selected" {
  id = element(data.aws_subnets.public.ids, 0)
}

data "aws_security_group" "selected" {
  filter {
    name   = "tag:Name"
    values = ["ssh-http-https"]
  }
}